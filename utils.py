# -*- coding: utf-8 -*-

import datetime
import email.header
import email.utils
import time

ESCAPING = (
    ('%', '%percent%'),
    ('/', '%slash%'),
)

default_date = datetime.datetime.fromtimestamp(0)



def escape_id(message_id):
    result = message_id
    for (match, replacement) in ESCAPING:
        result = result.replace(match, replacement)
    return result

def unescape_id(message_id):
    result = message_id
    for (replacement, match) in reversed(ESCAPING):
        result = result.replace(match, replacement)
    return result



def get_article_date(message):
    date_header = message['Date']
    if not date_header:
        return default_date
    time_tuple = email.utils.parsedate(date_header)
    if time_tuple is None:
        return default_date
    return datetime.datetime.fromtimestamp(time.mktime(time_tuple))

def get_encoded_header(message, header):
    header = message.get(header, '')
    chunks = email.header.decode_header(header)
    for i, (chunk, encoding) in enumerate(chunks):
        try:
            chunks[i] = chunk.decode(encoding or 'ascii', errors='ignore')
        except LookupError:
            chunks[i] = chunk.decode('ascii', errors='ignore')
    return ''.join(chunks)
