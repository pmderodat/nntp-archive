#! /usr/bin/python
# -*- coding: utf-8 -*-


import email.parser
import email.utils
import os, os.path

import utils



class ThreadsBuilder(object):
    def __init__(self, articles_directory):
        self.parser = email.parser.Parser()
        self.articles_directory = articles_directory
        self.threads = {}
        self.visited_articles = {}

    def get_filename(self, message_id):
        return os.path.join(
            self.articles_directory,
            utils.escape_id(message_id)
        )

    def get_parent_thread(self, parent_id):
        path = []
        while parent_id is not None:
            path.append(parent_id)
            parent_id = self.visited_articles[parent_id]
        thread = self.threads
        while path:
            msg_id = path.pop()
            thread = thread[msg_id]
        return thread

    def visit_article(self, msg_id):
        if msg_id in self.visited_articles:
            return self.get_parent_thread(msg_id)

        self.visited_articles[msg_id] = None
        try:
            msg_file = open(self.get_filename(msg_id), 'r')
        except (IOError, OSError):
            self.visited_articles.pop(msg_id)
            return None
        msg = self.parser.parse(msg_file)
        msg_file.close()

        references = sum((
            [email.utils.parseaddr(chunk)[1] for chunk in header.split()]
            for header in msg.get_all('References', [])), []
        )
        in_reply_to = msg.get_all('In-Reply-To')
        if in_reply_to:
            references += in_reply_to
        if references:
            _, in_reply_to = email.utils.parseaddr(references[-1])
        else:
            in_reply_to = None
        if not in_reply_to:
            thread = {}
            self.threads[msg_id] = thread
            return thread
        else:
            parent_thread = self.visit_article(in_reply_to)
            if parent_thread is None:
                # The parent article does not exist
                thread = {}
                self.threads[msg_id] = thread
                return thread
            self.visited_articles[msg_id] = in_reply_to
            new_thread = {}
            parent_thread[msg_id] = new_thread
            return new_thread



if __name__ == '__main__':
    import pickle
    import sys

    # TODO: arguments handling and usage
    articles_dir = sys.argv[1]
    threads_builder = ThreadsBuilder(articles_dir)
    for filename in os.listdir(articles_dir):
        msg_id = utils.unescape_id(filename)
        threads_builder.visit_article(msg_id)

    pickle.dump(threads_builder.threads, sys.stdout)
