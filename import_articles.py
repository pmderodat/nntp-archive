#! /usr/bin/env python
# -*- coding: utf-8 -*-

'''
For each filename found in stdin, try to parse the contained Usenet article and
copy it in the current directory using its Message-Id as a name.

Any existing article with the same Message-ID is replaced.

Problems are logged to stdout.
'''

import email.errors
import email.parser
import email.utils
import os.path
import shutil
import sys

import utils



parser = email.parser.Parser()

for filename in sys.stdin.readlines():
    filename = filename.strip()
    msg_file = open(filename, 'r')

    try:
        try:
            msg = parser.parse(msg_file)
        finally:
            msg_file.close()
    except email.errors.MessageError:
        print '%s is malformed. Parsing failed' % filename
        continue
    msg_id = msg['Message-Id']
    if msg_id is None:
        print '%s has no Message-Id' % filename
        continue
    realname, address = email.utils.parseaddr(msg_id)
    if not address:
        print '%s has invalid Message-Id: %s' % (filename, address)
        continue
    new_filename = utils.escape_id(address)
    if os.path.exists(new_filename):
        print '%s already exists. Replacing it.' % new_filename
    shutil.copy(filename, new_filename)
