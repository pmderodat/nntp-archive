#! /usr/bin/env python
# -*- coding: utf-8 -*-

import cgi
import collections
import email.errors
import email.parser
import email.utils
import os
import pickle
import sys
import urllib

import utils



parser = email.parser.Parser()

Thread = collections.namedtuple('Thread', (
    'date', 'id', 'subject', 'author', 'message', 'subthreads'
))

class SiteBuilder(object):
    encoding = 'utf-8'
    threads_directory = 'thread'

    def __init__(self, articles_directory, threads):
        self.articles_directory = articles_directory
        self.threads = self.prepare_threads(threads)

    def get_filename(self, message_id):
        return os.path.join(
            self.articles_directory,
            utils.escape_id(message_id)
        )

    def get_thread_filename(self, message_id):
        return os.path.join(
            self.threads_directory,
            '%s.html' % utils.escape_id(message_id)
        )

    def get_message(self, message_id):
        msg_file = open(self.get_filename(message_id), 'r')
        message = parser.parse(msg_file)
        msg_file.close()
        return message

    def prepare_threads(self, threads):
        result = []
        for message_id, subthreads in threads.iteritems():
            message = self.get_message(message_id)
            date = utils.get_article_date(message)
            subject = utils.get_encoded_header(message, 'Subject')
            realname, address = email.utils.parseaddr(
                utils.get_encoded_header(message, 'From')
            )
            if realname:
                if '=?' in realname:
                    # This is some kind of hack, because some inner-realname
                    # are not properly parsed by get_encoded_header, for
                    # instance:
                    #    From: email@addr.ess (=?utf-8?Q?Name?=)
                    author = utils.get_encoded_string(
                        realname.encode('utf-8', errors='ignore'))
                else:
                    author = realname
            else:
                author = u'%s@…' % address.split('@')[0]
            result.append(Thread(
                date=date, id=message_id, subject=subject, author=author,
                message=message, subthreads=self.prepare_threads(subthreads)
            ))
        result.sort()
        return result

    def build(self):
        index = open('index.html', 'w')
        index.write(
            '<html>'
            '<head>'
            '<meta http-equiv="Content-Type" content="text/html; charset=%s" />'
            '<link rel="StyleSheet" type="text/css" href="style.css" />'
            '</head>'
            '<body><ul>\n' % self.encoding
        )
        for thread in self.threads:
            self.build_thread(thread)
            index.write((u'<li><a href="%s">%s - %s - %s - %s</a></li>\n' % (
                self.get_thread_filename(thread.id),
                thread.message['Newsgroups'],
                thread.date, cgi.escape(thread.author),
                cgi.escape(thread.subject)
            )).encode(self.encoding))
        index.write('</ul></body></html>\n')
        index.close()

    def build_thread(self, thread):
        articles = open(self.get_thread_filename(thread.id), 'w')
        articles.write(
            '<html>'
            '<head>'
            '<meta http-equiv="Content-Type" content="text/html; charset=%s" />'
            '<link rel="StyleSheet" type="text/css" href="../style.css" />'
            '</head>'
            '<body>\n' % self.encoding
        )
        articles.write((
            u'<h1>%s</h1>' % cgi.escape(thread.subject)
        ).encode(self.encoding))

        def write_anchors(thread):
            articles.write((u'<li><a href="#%s">%s - %s - %s</a></li>\n' % (
                urllib.quote(thread.id),
                thread.date, cgi.escape(thread.author),
                cgi.escape(thread.subject)
            )).encode(self.encoding))
            if thread.subthreads:
                articles.write('<ul>\n')
                for subthread in thread.subthreads:
                    write_anchors(subthread)
                articles.write('</ul>\n')
        articles.write('<ul>\n')
        write_anchors(thread)
        articles.write('</ul>\n')

        def write_article(thread):
            articles.write((u'<div class="article"><h2 id="%s">%s</h2>\n' % (
                cgi.escape(thread.id, quote=True), cgi.escape(thread.subject)
            )).encode(self.encoding))
            articles.write((u'<p>%s - %s</p>\n' % (
                thread.date, cgi.escape(thread.author)
            )).encode(self.encoding))
            if thread.message.is_multipart():
                articles.write((
                    u'<p>Multipart message: not (yet) supported!</p>'
                ).encode(self.encoding))
            else:
                # TODO: detect the proper charset
                content = thread.message.get_payload().decode('utf-8', errors='ignore')
                content = self.colorize_article(content)
                articles.write((
                    u'<pre>%s</pre>\n' % content
                ).encode(self.encoding))
            articles.write('</div>\n')
            for subthread in thread.subthreads:
                write_article(subthread)
        write_article(thread)

        articles.write('</body></html>\n')
        articles.close()

    def colorize_article(self, content):
        lines = content.split(u'\n')
        result = []
        reached_signature = False
        in_quoted_block = False
        for line in lines:
            eline = cgi.escape(line)
            if reached_signature:
                line = eline
            elif line == u'-- ':
                if in_quoted_block:
                    result[-1] += u'</span>'
                reached_signature = True
                line = u'<span class="signature">%s' % eline
            elif len(line) >= 1 and line[0] in u'>}':
                if not in_quoted_block:
                    in_quoted_block = True
                    line = u'<span class="quotation">%s' % eline
                else:
                    line = eline
            elif in_quoted_block:
                in_quoted_block = False
                result[-1] += u'</span>'
                line = eline
            else:
                line = eline
            result.append(line)

        if reached_signature or in_quoted_block:
            result[-1] += u'</span>'
        return u'\n'.join(result)

if __name__ == '__main__':
    import pickle
    import sys

    articles_dir = sys.argv[1]
    threads = pickle.load(sys.stdin)
    site_builder = SiteBuilder(articles_dir, threads)
    site_builder.build()
